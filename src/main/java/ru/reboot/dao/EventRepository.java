package ru.reboot.dao;

import ru.reboot.dao.entity.EventEntity;

import java.util.List;

public interface EventRepository {

    List<EventEntity> getAllUserEvents(String userId);

    List<EventEntity> getUserEventsByDate(String userId, String date);

    EventEntity getEvent(String eventId);

    EventEntity deleteEvent(String eventId);

    EventEntity cancelEvent(String eventId);

    EventEntity finishEvent(String eventId);

    EventEntity createEvent(EventEntity event);

    EventEntity updateEvent(EventEntity event);

    void deleteUserEventsByDate(String userId, String date);

    List<EventEntity> saveAllEvents(String userId,
                                    String date,
                                    List<EventEntity> events);

    List<EventEntity> getNearestEvents(int delta);
}