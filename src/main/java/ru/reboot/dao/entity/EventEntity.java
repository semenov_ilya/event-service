package ru.reboot.dao.entity;

import ru.reboot.dto.EventPriority;
import ru.reboot.dto.EventState;

import javax.persistence.*;

@Entity
@Table(name = "events")
public class EventEntity {

    @Id
    @Column
    private String id;

    @Column(name = "user_id")
    private String userId;

    @Column
    private String name;

    @Column
    private String description;

    @Column()
    private String date;

    @Column()
    private String time;

    @Column(columnDefinition = "VARCHAR")
    @Enumerated(EnumType.STRING)
    private EventState state;

    @Column(columnDefinition = "VARCHAR")
    @Enumerated(EnumType.STRING)
    private EventPriority priority;

    public EventEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public EventState getState() {
        return state;
    }

    public void setState(EventState state) {
        this.state = state;
    }

    public EventPriority getPriority() {
        return priority;
    }

    public void setPriority(EventPriority priority) {
        this.priority = priority;
    }



}
