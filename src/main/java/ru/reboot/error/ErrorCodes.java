package ru.reboot.error;

public enum ErrorCodes {
    USER_NOT_FOUND,
    ILLEGAL_ARGUMENT,
    EVENT_NOT_FOUND,
    EVENT_ALREADY_EXISTS,
    SAVE_ALL_EVENTS_ERROR,
    GET_EVENT_BY_EVENT_ID_ERROR,
    CREATE_EVENT_ERROR,
    DELETE_USER_EVENTS_BY_DATE,
    DELETE_EVENT_BY_EVENT_ID_ERROR,
    CANCEL_EVENT_BY_EVENT_ID_ERROR
}
