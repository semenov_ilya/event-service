package ru.reboot.service;

import ru.reboot.dto.EventDTO;

import java.util.List;

public interface EventService {

    /**
     * Получить список всех событий пользователя
     *
     * @param userId - user id
     * @throws USER_NOT_FOUND exception if user not found
     */
    List<EventDTO> getAllUserEvents(String userId);

    /**
     * Получить список всех событий пользователя за определенную дату
     *
     * @param userId - user id
     * @throws USER_NOT_FOUND exception if user not found
     */
    List<EventDTO> getUserEventsByDate(String userId, String date);

    /**
     * Получить информацию о конкретном событии
     *
     * @param eventId - event id
     * @throws EVENT_NOT_FOUND if event doesn't exists
     */
    EventDTO getEvent(String eventId);

    /**
     * Удалить информацию о событии.
     *
     * @param eventId - event id
     * @throws EVENT_NOT_FOUND if event doesn't exists
     */
    EventDTO deleteEvent(String eventId);

    /**
     * Отменить событие.
     *
     * @param eventId - event id
     * @throws EVENT_NOT_FOUND if event doesn't exists
     */
    EventDTO cancelEvent(String eventId);

    /**
     * Завершить событие.
     *
     * @param eventId - event id
     * @throws EVENT_NOT_FOUND if event doesn't exists
     */
    EventDTO finishEvent(String eventId);

    /**
     * Создать событие.
     *
     * @param event - event
     * @throws EVENT_ALREADY_EXISTS if event already exists
     */
    EventDTO createEvent(EventDTO event);

    /**
     * Создать событие.
     *
     * @param event - event
     * @throws EVENT_NOT_FOUND if event doesnt exist
     */
    EventDTO updateEvent(EventDTO event);

    /**
     * Удалить все события пользователя за определенную дату
     *
     * @param userId - user id
     * @param date-  date
     * @throws USER_NOT_FOUND exception if user not found
     */
    void deleteUserEventsByDate(String userId, String date);

    /**
     * Сохранить список событий
     *
     * @param events - list of events
     */
    List<EventDTO> saveAllEvents(String userId,
                                 String date,
                                 List<EventDTO> events);

    /**
     * Получить информацию о предстоящих событиях всех пользователей
     *
     * @param delta - время в seconds
     */
    List<EventDTO> getNearestEvents(int delta);
}
