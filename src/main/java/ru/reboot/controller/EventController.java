package ru.reboot.controller;

import ru.reboot.dto.EventDTO;

import java.util.List;

public interface EventController {

    List<EventDTO> getAllUserEvents(String userId);

    List<EventDTO> getUserEventsByDate(String userId, String date);

    EventDTO getEvent(String eventId);

    EventDTO deleteEvent(String eventId);

    EventDTO cancelEvent(String eventId);

    EventDTO finishEvent(String eventId);

    EventDTO createEvent(EventDTO event);

    EventDTO updateEvent(EventDTO event);

    void deleteUserEventsByDate(String userId, String date);

    List<EventDTO> saveAllEvents(String userId,
                                 String date,
                                 List<EventDTO> events);

    List<EventDTO> getNearestEvents(int delta);
}
